\documentclass{MHM2_lectures}

\newcommand{\vek}[1]{{\boldsymbol #1}}
\newcommand{\phs}[1]{^{(#1)}}

\begin{document}
    
\maketitle{1}{Principles of "mathematical" homogenization}

\tableofcontents

\section{Recap of governing equations}
%
\begin{itemize}\noitemsep
    \item domain $\Omega \subseteq \mathbb{R}^d$
    \item position $\vek{x} \in \Omega$
    \item state (variable) $u: \Omega \rightarrow \mathbb{R}$~(test $\varphi$) 
    \item intensity field $\vek{e} : \Omega \rightarrow \mathbb{R}^d$~(test $\vek v$) 
    \item flux field $\vek{\jmath} : \Omega \rightarrow \mathbb{R}^d$~(test $\vek q$)
\end{itemize}

\paragraph{Field equations}

\begin{align*}
    \vek{e}( \vek{x} ) = - \vek{\nabla} u( \vek{x}),
    &&
    \vek{\nabla}^T \vek{\jmath}( \vek{x} ) = b( \vek{x} ),
    &&
    \vek{\jmath}( \vek{x} ) = \vek{L}( \vek{x} ) \vek{e}( \vek{x} )
    &&
    \text{for }
    \vek{x} \in \Omega
\end{align*}

\paragraph{Governing equations and boundary conditions}

\begin{align*}
    - \vek{\nabla}^T 
    \left( 
        \vek{L}( \vek{x} )
        \vek{\nabla} u (\vek{x} )
    \right)
    & =
    b( \vek{x} )
    \text{ for } \vek{x} \in \Omega
    \\
    u( \vek{x} ) & = 0 
    \text{ for } \vek{x} \in \partial \Omega
\end{align*}

\section{Setting of periodic homogenization}
%
\begin{itemize}
    \item Unit cell $\mathcal{Y} = [-\frac{1}{2}, \frac{1}{2}]^d$
    \item Scale parameter $\varepsilon > 0$
    \item Coordinate decomposition
    %
    \begin{align*}
        \frac{\vek{x}}{\varepsilon} 
        =
        \left\lfloor 
            \frac{\vek x}{\varepsilon} 
        \right\rfloor_{\mathcal Y}
        +
        \left\{ 
            \frac{\vek x}{\varepsilon} 
        \right\}_{\mathcal Y}
    \end{align*}

    \item $\vek{L}^\varepsilon( \vek{x} ) = \vek{L}\left(         \left\{ 
        \frac{\vek x}{\varepsilon} 
    \right\}_{\mathcal Y} \right) = \vek{L}_\mathrm{per}( \vek{x} / \varepsilon )$ is $\mathcal{Y}$-periodic

    \item On the unit cell level 
    %
    \begin{align*}
        \vek{L}_\mathrm{per}( \vek{y} )
        =
        \sum_{r=1}^N \chi_\mathrm{per}\phs{r}( \vek{y} ) \vek{L}\phs{r},
        \quad
        \vek{y} \in \mathcal{Y}
    \end{align*}

    \item Defines $\varepsilon$-parameterized solution $u^\varepsilon$
    
    \item How to quantify their behavior as $\varepsilon \rightarrow 0$?

\end{itemize}

\section{Asymptotic expansion}

\begin{itemize}
    \item Two-scale ansatz of the solution in the form
    %
    \begin{align*}
        u^\varepsilon( \vek{x} )
        \approx
        u^0\left( \vek{x}, \vek{y} \right) 
        \big\vert_{\vek{y} = \vek{x} / \varepsilon} 
        +
        \varepsilon \,
        u^1\left( \vek{x}, \vek{y} \right)
        \big\vert_{\vek{y} = \vek{x} / \varepsilon} 
    \end{align*}
    %
    after~\cite{Bensoussan1978}, \cite{Sanchez-Palencia1980}
 
    \item "Two-scale calculus"~(chain rule); $i = 1, \ldots, d$
    %
    \begin{align*}
        \nabla_i \, f( \vek{x}, \vek{y} )
        \big\vert_{\vek{y} = \vek{x} / \varepsilon} 
        =
        \frac{\partial}{\partial x_i} f( \vek{x}, \vek{y} )
        +
        \vek{\nabla}_{\vek y} f( \vek{x}, \vek{y} )
        \frac{\partial \vek{y}}{\partial x_i}
        =
        \frac{\partial}{\partial x_i} f( \vek{x}, \vek{y} )
        +
        \frac{1}{\varepsilon}
        \frac{\partial}{\partial y_i} f( \vek{x}, \vek{y} )
    \end{align*}

    \item Intensity field
    %
    \begin{align*}
        - \vek{e}^\varepsilon( \vek{x} ) 
        & = 
        \vek{\nabla} u^\varepsilon( \vek{x})
        \approx 
        \left( 
            \vek{\nabla}_{\vek x}
            u^0\left( \vek{x}, \vek{y} \right)
            +
            \varepsilon^{-1}
            \vek{\nabla}_{\vek y}
            u^0\left( \vek{x}, \vek{y} \right)
        \right)
        + \varepsilon
        \left( 
            \vek{\nabla}_{\vek x}
            u^1\left( \vek{x}, \vek{y} \right)
            +
            \varepsilon^{-1}
            \vek{\nabla}_{\vek y}
            u^1\left( \vek{x}, \vek{y} \right)
        \right)
        \\
        & = 
        \varepsilon^{-1}
        \vek{\nabla}_{\vek y}
        u^0\left( \vek{x}, \vek{y} \right)
        +
        \left(
        \vek{\nabla}_{\vek x}
        u^0\left( \vek{x}, \vek{y} \right)
        +
        \vek{\nabla}_{\vek y}
        u^1\left( \vek{x}, \vek{y} \right)
        \right)
        +
        \varepsilon
        \vek{\nabla}_{\vek x}
        u^1\left( \vek{x}, \vek{y} \right)
    \end{align*}

    \item Flux field
    %
    \begin{align*}
        \vek{\jmath}^\varepsilon( \vek{x} )
        & =
        \vek{L}^\varepsilon( \vek{x} )
        \vek{e}^\varepsilon( \vek{x} )
        \approx 
        -
        \vek{L}( \vek{y} )
        \left( 
            \varepsilon^{-1}
            \vek{\nabla}_{\vek y}
            u^0\left( \vek{x}, \vek{y} \right)
            +
            \left(
            \vek{\nabla}_{\vek x}
            u^0\left( \vek{x}, \vek{y} \right)
            +
            \vek{\nabla}_{\vek y}
            u^1\left( \vek{x}, \vek{y} \right)
            \right)
            +
            \varepsilon
            \vek{\nabla}_{\vek x}
            u^1\left( \vek{x}, \vek{y} \right)    
        \right)
    \end{align*}

    \item Conservation condition
    %
    \begin{align*}
        b( \vek{x} ) 
        = 
        \vek{\nabla}^\mathsf{T} \vek{\jmath}^\varepsilon( \vek{x} )
        \approx
        - \vek{\nabla}_{\vek x}^\mathsf{T} \left[ \bullet \right]
        - \varepsilon^{-1} \vek{\nabla}_{\vek y}^\mathsf{T} \left[ \bullet \right]
    \end{align*}

\end{itemize}

\section{Hierarchy of governing equations for $\varepsilon^{ \{ -2, -1, 0 \} }$}

\begin{align*}
    \varepsilon^{-2} :     
    && 
    - \vek{\nabla}_{\vek y}^\mathsf{T} 
    \left[ 
        \vek{L}( \vek{y} )
        \vek{\nabla}_{\vek y}
        u^0\left( \vek{x}, \vek{y} \right)
    \right]
    & = 0 
    \\
    \varepsilon^{-1} : 
    &&
    - \vek{\nabla}_{\vek x}^\mathsf{T} \left[ 
        \vek{L}( \vek{y} )
        \vek{\nabla}_{\vek y}
        u^0\left( \vek{x}, \vek{y} \right)
    \right]
    - \vek{\nabla}_{\vek y}^\mathsf{T} \left[
        \vek{L}( \vek{y} )
        \left(
        \vek{\nabla}_{\vek x}
        u^0\left( \vek{x}, \vek{y} \right)
        +
        \vek{\nabla}_{\vek y}
        u^1\left( \vek{x}, \vek{y} \right)
    \right)
    \right]
    & = 0
    \\
    \varepsilon^{0} : 
    &&
    - \vek{\nabla}_{\vek x}^\mathsf{T} \left[
        \vek{L}( \vek{y} )
        \left(
        \vek{\nabla}_{\vek x}
        u^0\left( \vek{x}, \vek{y} \right)
        +
        \vek{\nabla}_{\vek y}
        u^1\left( \vek{x}, \vek{y} \right)
    \right)
    \right]
    - \vek{\nabla}_{\vek y}^\mathsf{T} \left[
        \vek{L}( \vek{y} )
        \vek{\nabla}_{\vek x}
        u^1\left( \vek{x}, \vek{y} \right)
    \right]
    & = b( \vek{x} )
\end{align*}
%
for $\vek{x} \in \Omega$ and $\vek{y} \in \mathcal{Y}$.

\paragraph{Homework}

\begin{itemize}\noitemsep
    \item Justification of fast/small variables
    \item Develop hierarchy of equations~(according to time available)
\end{itemize}

\Reference

\Acknowledgement

\end{document}