#!/bin/bash

dirs=( jupyter-dohnalen jupyter-gaynuliy jupyter-horakma2 jupyter-smejkmi2 jupyter-zemanjan)
files=( lecture08.ipynb )
sourceFolder="jupyter-doskamar"

for curDest in "${dirs[@]}"
do
	for curFile in "${files[@]}"
	do
		cp "/home/$sourceFolder/$curFile" "/home/$curDest/$curFile"
	done
done
