# D32_HM2_EN

Repository for supporting materials for the Ph.D. course "D32MH2: Micromechanics of Heterogeneous Materials II (Numerical Methods)", taught at the Faculty of Civil Engineering, Czech Technical University in Prague.

The first version of the course materials was prepared with the support of the European Social Fund and the State Budget of the Czech Republic under project No. CZ.02.2.69/0.0/0.0/16_018/0002274.

![OPVVV logolink](https://opvvv.msmt.cz/media/msmt/uploads/OP_VVV/Pravidla_pro_publicitu/logolinky/JVS2_opraveny_aj/logolink_OP_VVV_hor_barva_eng.jpg)

## Steps to run Julia in Jupyter Notebooks
```
julia
using Pkg
Pkg.add("IJulia")
Pkg.add("StaticArrays")
```
```
sudo cp fea.jl/ ../jupyter-[ADD_USERNAME_HERE]/fea.jl -r
sudo cp lecture04.ipynb ../jupyter-[ADD_USERNAME_HERE]/lecture[ADD_LECTURE_NUMBER_HERE].ipynb -r
sudo chmod a+w ../jupyter-[ADD_USERNAME_HERE]/lecture[ADD_LECTURE_NUMBER_HERE].ipynb
```
