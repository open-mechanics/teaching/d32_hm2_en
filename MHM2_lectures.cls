\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{MHM2_lectures}[2022/02/08 Course lecture notes]

\LoadClass[11pt]{article}

%% Required packages
\RequirePackage[a4paper,margin=2cm]{geometry}
\RequirePackage[utf8]{inputenc}
\RequirePackage[numbers]{natbib}
\RequirePackage{graphicx}
\RequirePackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\RequirePackage{fancyhdr}
\RequirePackage{pdfpages}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}

%% Bibliography setup
\bibliographystyle{elsarticle-harv}

%% Graphicx setup
\graphicspath{{figures/}}

%% Other commands
\newcommand{\noitemsep}{\itemsep=0pt}

\newcounter{LectureNumber}
%\newcounter{CisloPrikladu}

\renewcommand{\maketitle}[2]{%
   \setcounter{LectureNumber}{#1}%
    %% Facyhdr setup
    \pagestyle{fancy}%
    \fancyhf{}%
    \chead{D32\_MHM2\_EN | Lecture~\Roman{LectureNumber}: #2}%
    \rfoot{\thepage}%
}

\newcommand{\Reference}{%
\bibliography{liter}
}

\newcommand{\Acknowledgement}{%
%
\vfill
%
\paragraph{Acknowledgement}
%
\begin{center}
    \includegraphics[scale=0.85]{figures/logolink_OP_VVV_hor_barva_eng}
\end{center}
%
The first version of the course materials was prepared with the support of the European Social Fund and the State Budget of the Czech Republic under project No. CZ.02.2.69/0.0/0.0/16\_018/0002274. 

\begin{center}
    \includegraphics[scale=0.80]{figures/CC_BY_4_0}
\end{center}    
%
This work is licensed under the \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons Attribution 4.0 International License}.
}